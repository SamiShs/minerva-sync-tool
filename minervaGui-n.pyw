from tkinter import *
import requests
import os
from lxml import html

def update_main():
    sublist = []
    counter = 0
    window = Tk()
    window.wm_title('bezig...')
    proces = Label(window,text='bezig met bijwerken...',height=3,width=25,fg='green',font=("Courier", 20))
    proces.pack()
    window.update()
    with open('%s/config' % os.path.dirname(__file__),'r') as config:
        data = config.read().splitlines()
        username,password = data[0],data[1]
        session = requests.Session()
        session.headers.update(
            {'user-agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1'})
        session.post(
            'https://login.ugent.be/login?service=https%3A%2F%2Fminerva.ugent.be%2Fplugin%2Fcas%2Flogincas.php',
            data={'username': username, 'password': password})
        data = data[2:len(data)]
        vak = []
        vakcode = []
        for i in range(0,len(data),2):
            vak.append(data[i])
            vakcode.append(data[i+1])
        code = []
        for i in vakcode:
            code.append(i.split('=')[1])
    for i in code:
        urldoc = 'http://minerva.ugent.be/main/document/document.php?cidReq='+i
        def make_list(urlz,folder,count):
            page = session.get(urlz)
            tree = html.fromstring(page.text)
            folders = list(set(tree.xpath("//tbody/tr/td/div/span/a[@title]/@title")))
            for u in folders:
                j = u.replace(':','-')
                if '.' in j:
                    tmppath = folder +'/'+ j.split('/')[-1]
                    if not os.path.isfile('%s/%s' % (os.path.dirname(__file__),tmppath)):
                        tmurl = list(set(tree.xpath("//tbody/tr/td/div/span/a[@title='%s']/@href" % u)))[0]
                        if 'http' in tmurl:
                            file_url = session.get(tmurl)
                            with open('%s/%s' % (os.path.dirname(__file__),tmppath) , "wb") as codes:
                                codes.write(file_url.content)
                            count += 1
                            sublist.append(j.split('/')[-1])
                else:
                    if not os.path.exists('%s/%s/%s/' % (os.path.dirname(__file__),folder,j)):
                        os.makedirs('%s/%s/%s/' % (os.path.dirname(__file__),folder,j))
                    tmpurl = 'http://minerva.ugent.be/main/document/'+list(set(tree.xpath("//tbody/tr/td/div/span/a[@title='%s']/@href" %u)))[0]
                    count=make_list(tmpurl,folder+'/'+j,count)
            return count
        tmp_p = vak[code.index(i)]
        tmp_p = tmp_p.replace(':','-')
        counter=make_list(urldoc, 'minerva/%s' % tmp_p,counter)
    def ok():
        window.destroy()
    def details():
        ok()
        def oks():
            window.destroy()
        window = Tk()
        window.wm_title('bestanden')
        Label(window, text='Deze ' + str(counter) + ' bestanden werden gedownload :)', height=3, width=40, fg='green',
              font=("Courier", 15)).grid(row=0,columnspan=3)
        z = 1
        for i in sublist:
            Label(window,text=str(z)+': ').grid(column = 0,row=z,sticky=E)
            Label(window,text=i).grid(column = 1,row=z,sticky=W)
            z += 1
        Button(window, text='OK', command=oks, height=2, width=20).grid(column=1,sticky=W)

        window.mainloop()

    proces.pack_forget()
    Label(window,text=str(counter)+' bestanden bijgewerkt :)',height=3,width=25,fg='green',font=("Courier", 20)).pack()
    if counter >0 and counter <20:
        Button(window, text='details', command=details, height=2, width=20).pack()
    Button(window,text='OK',command=ok,height=2, width=20).pack()
    window.mainloop()
def setup():
    def login():
        username = entry_1.get()
        password = entry_2.get()
        session = requests.Session()
        session.headers.update(
            {'user-agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1'})
        session.post(
            'https://login.ugent.be/login?service=https%3A%2F%2Fminerva.ugent.be%2Fplugin%2Fcas%2Flogincas.php',
            data={'username': username, 'password': password})
        homepage = session.get('http://minerva.ugent.be/main/curriculum/index.php?view=coursesite')
        tree = html.fromstring(homepage.text)
        subjects = list(set(tree.xpath("//tbody/tr/td/a[@title]/@title")))
        if len(subjects) == 0:
            def fale():
                fout.destroy()
            fout = Tk()
            fout.title(':/')
            Label(fout,text='foutieve inloggegevens bro',fg='red',height=5,width=35,font=("Courier", 15)).pack()
            Button(fout,text='OK',height=2, width=20,command=fale).pack()
            fout.mainloop()
        else:
            def save():
                open('%s/config' % os.path.dirname(__file__), 'w').close()
                with open('%s/config' % os.path.dirname(__file__), 'w') as config:
                    config.writelines(username)
                    config.writelines('\n')
                    config.writelines(password)
                    config.writelines('\n')
                    for i in range(len(check_subjects)):
                        if check_subjects[i].get():
                            config.writelines(subjects[i])
                            config.writelines('\n')
                            config.writelines(list(set(tree.xpath("//tbody/tr/td/a[@title='%s']/@href" %subjects[i])))[0])
                            config.writelines('\n')
                            if not os.path.exists('%s/minerva' % os.path.dirname(__file__)):
                                os.makedirs('%s/minerva' % os.path.dirname(__file__))
                            for i in range(len(check_subjects)):
                                if check_subjects[i].get():
                                    u = subjects[i]
                                    u = u.replace(':', '-')
                                    if not os.path.exists('%s/minerva/%s/' % (os.path.dirname(__file__),u)):
                                        os.makedirs('%s/minerva/%s/' % (os.path.dirname(__file__),u))
                    button_update.config(state='normal')
            check_subjects = []
            for i in range(len(subjects)):
                check_subjects.append(IntVar())
                Checkbutton(root,text=subjects[i], variable=check_subjects[i]).grid(column=0,columnspan=3, sticky=W)
            Button(root, text='bewaar gegevens',command=save).grid(row=2,column=1)
    Label(root, fg='red', text='gebruikersnaam').grid(row=0,column=2, sticky=E)
    Label(root, fg='red', text='wachtwoord').grid(row=1,column=2, sticky=E)
    entry_1 = Entry(root)
    entry_2 = Entry(root,show='*')
    entry_1.grid(row=0, column=3)
    entry_2.grid(row=1, column=3)
    Button(root,text='inloggen', command=login,height=2, width=20).grid(row=2,column=2,columnspan=2)

root = Tk()
root.title('Minerva bijwerken')
button_gegevens = Button(root,text='instellen',command=setup,height=2, width=20).grid(row=0,column=0)
if os.path.isfile('%s/config' % os.path.dirname(__file__)):
    button_update = Button(root, text='bijwerken',height=2, width=20,command=update_main)
    button_update.grid(row=0,column=1)
else:
    button_update = Button(root, text='bijwerken', state=DISABLED,height=2, width=20,command=update_main)
    button_update.grid(row=0,column=1)
root.mainloop()
